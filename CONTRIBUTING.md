Experts, just send a pull request or a merge request.

Novices, enter Issues:
- [Bash Programming issues here](https://gitlab.com/sofreeus/bash-programming/issues)
- [SFS issues here](https://gitlab.com/sofreeus/sofreeus/issues)
