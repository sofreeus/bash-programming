Talk
===

What is bash?

Why choose bash?

Demo
===

- Alluvial programming
  + What is the mast popular shell on this system?
  + Hello Bob! Would you like to play a game?

- Stupid history tricks
  + [Control]+[R], pattern
  + `history | less`, [Control]+[LeftMouse]+Drag
  + grep `~/.bash_history`, HISTFILESIZE, HISTLENGTH

- Finally, saving and editing program files
  + Hello Bob! Would you like to play a game?
  + What is the mast popular shell on this system?

[Session 0 Recording](https://www.youtube.com/watch?v=Z0jmoCGvs6Q)
