Use alluvial programming for these:

Find your words file in /usr or get one online.

`locate words | grep usr | grep 'dict/words$' # build a pipe to find it.`

How many words are there in the words file?

`cat $(locate words | grep usr | grep 'dict/words$') | wc -l # count its words`

How many words in words have 'e' or 'E' as the second letter?

`grep '^.e' $(locate words | grep usr | grep 'dict/words$') | wc -l # filter before the pipe!`

`grep -i '^.e' $(locate words | grep usr | grep 'dict/words$') | wc -l # make it case-insensitive`


===
Make a list of our names and hello us all

`cat names | while read i; do echo $i; done # start trivially`

`cat names | while read i; do echo hello $i; done # stick in hellos`

How many of our names have e as the second letter?

`cat names | while read i; do echo $i; done | egrep '^.e' # filter for 'e'`

How about a or e?

`cat names | while read i; do echo $i; done | egrep '^.(a|e)' # look for 'a' or 'e'`

Do any of our names not have a vowel in the second position?

`cat names | while read i; do echo $i; done | egrep '^.[aeiouy]' # look for any vowel`

`cat names | while read i; do echo $i; done | egrep -v '^.[aeiouy]' # look for any non-vowel!`
