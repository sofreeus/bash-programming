Talk
===

What is bash?

- a POSIX-conforming programmable shell
- consider also: zsh, ksh, pash, sh, dash, etc...
(And only if, by "pash" you mean the parallelizing shell, which is a layer on top of bash.)

Why choose bash?

- ubiquity
- better than most
- about as good as the rest
- POSIX-conforming (that is, there's a portable subset).


Demo
===

- Alluvial programming

What is the most popular shell on this system?

Don't open a text editor until you have your program working.
Grow your code a step at a time.

```
cat /etc/passwd
cat /etc/passwd | awk -F: '{print $NF}'
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#'
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#' | sort
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#' | sort | uniq -c
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#' | sort | uniq -c | sort -nr 
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#' | sort | uniq -c | sort -nr | head -1
cat /etc/passwd | awk -F: '{print $NF}' | grep -v '^#' | sort | uniq -c | sort -nr | head -1 | awk '{print $2}'
```

*Now* use fc to put the working code into a file and turn it into a script, something like `most-popular-shell`

Hello Bob! Would you like to play a game?

```bash
echo "Hello World!"
greeting=hello name=bob echo "$greeting $name\!"
greeting=hello name=bob ; echo $greeting $name\!
greeting=hello name=bob && echo $greeting $name\!
greeting=hello name=david && echo $greeting $name\!
greeting=hello name=$( read -p "name? " ) && echo $greeting $name\
greeting=hello ; read -p "name? " name && echo $greeting $name\!
help read
read -n 1 choice; echo choice
read -p "Would you like to play a game? " -n 1 choice; echo choice=$choice
if [[ $choice =~ [yY] ]]; then echo choice was like yes; else echo choice was not like yes; fi
```

- Stupid history tricks
  + [Control]+[R], pattern
  + `history | less`, [Control]+[LeftMouse]+Drag
  + grep `~/.bash_history`, HISTFILESIZE, HISTLENGTH

- Finally, some scripting
  + see hello-bob
  + see most-popular-shell
