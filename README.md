# B is for Bash

2021-09-10

- Go over what folks brought
    * Gary - zsh w ohmyzh and git plugin adds aliases and informational prompt in git repos
    * Jeff - add time to debugging output
    ```bash
    PS4='\t: '
    set -x
    # debuggables
    set +x
    ```

- Review the stuph in here:
    * Alluvial programming
    * Of you and yucks (bash -eu and bash -eux)
- Cover anything from the post that didn't already come up
- Return to what folks brought
- Write Issues for anything unsolved

# Bash Study Group

2018-04-18

Learn Bash programming by solving small, realistic problems that are well-suited to bash.

[Meetup](https://www.meetup.com/sofreeus/events/gqdnmpyxgbxb)

Online, instructor-led study group.

Starts Wednesday, April 18th at 7PM, and runs once a week, for 4 weeks

[Session0](Session0/) ran on Wenesdday, March 14th, Pi Day 2018

Schedule:

```
6:30 - 6:45: gather
6:45 -  7PM: Intros, Show&Tell, Questions
7PM  - 7:15: Talk: this weeks features
7:15 - 7:30: Demo: solve this week's problems
7:30 -  8PM: Pair Lab: participants solve this week's problems
8PM  - 8:15: Outros, Show&Tell, Questions
8:15 - 8:30: depart
```

Here is [how to pay](https://gitlab.com/sofreeus/sofreeus/blob/master/how-to-pay.md). Work with Heather if none of those options are good for you. Thank you in advance for rewarding me!
