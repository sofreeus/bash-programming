# Bash Programming, Session 3 of 4

Last week, Chris asked, "How can I send myself mail from my cron scripts?"

This leads to some interesting follow-on questions:
- How do I mail-enable my server? [mail-enable](mail-enable.md)
- What if I want to send a file?
- What is the default mail behavior of cron for users?
- What is the default mail behavior of cron for /etc/cron*?
- What if I want to override the default behavior?
- What if I want to redirect my mail, root's mail, another user's mail?
- But I want a local copy, too?
We probably won't get to all those questions. :-)

What's `byobu` and why is it awesome?

Ssh to your favorite server, run `byobu-enable`, log out, and ssh in again. Smell the awesome.

Run `byobu-disable` if you want to go back to normal.

From a `byobu` session, use [F6] to disconnect quickly. Lots more F keys.
