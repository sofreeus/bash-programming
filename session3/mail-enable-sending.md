# How to Mail-Enable a Server Machine

MUA = mail user agent, fetches delivered mail
MTA = mail transfer agent, accepts and delivers mail

There are three basic ways to mail-enable a machine.
They differ on how the machine sends remote mail.

All mail servers deliver locally, by default. One can `view` or `less` one's mail spool directly, or use the `mail` program to browse it interactively, or install various POP/IMAP/web-mail servers on the server machine and access the locally-delivered mail with a remote MUA.

Sending mail to remote domains is more complex, because spam filters guess.

The simplest configuration sends mail directly, but because most server machines have low/no "reputation", by default, this is also the method most likely to result in system messages being blocked by spam filters.

The MTA can also be configured to send mail through a local/trusting relay. If the relay is reputable, the system messages are much more likely to be delivered / less likely to be filtered.

Finally, the MTA can be configured to send mail through an authenticated relay. When there's no local, reputable relay, this may be the only option when delivery to remote domains is required, despite being the most complex.

## CentOS 7 Minimal

```bash
mail
yum provides "*bin/mail"
sudo yum install mailx
sudo systemctl status postfix
```

## Ubuntu Server 16.04

```bash
mail
sudo apt install mailutils
# "Internet Site"
# ~cromulent FQDN
```

## openSUSE 42.3 (server flavor)


## Simple mail-transfer tests

You're going to need a destination MTA that doesn't guess your messages of low-repute are spam.

```bash
echo "hello from $HOSTNAME" | mail -s test dlwillson@thegeek.nu
echo "hello from $HOSTNAME" | mail -s test dlwillson@sofree.us
echo "hello from $HOSTNAME" | mail -s test osadmin
ls -ltr /var/spool/mail/
less /var/spool/mail/$USER
```
