
The [ABS](http://tldp.org/LDP/abs/html/) Advanced Bash-Scripting Guide

```
I'm a huge fan of the ABS, and if I'm ever doing some intense scripting, that will work my brain, I'll always refer to it:

I'd probably start by using something similar to their chapter format:

Class 1: Basics (variables, exits, random stuff)
Class 2: Beyond the Basics (variables, substitutions, expansions, and loops)
Class 3: Commands
Class 4: Advanced Topics. . . (can extend this subject matter several days, potentially).
-- MikeDawg
```

@cfedde:

- http://tldp.org/LDP/Bash-Beginners-Guide/html/
- intro to linux: http://www.tldp.org/LDP/intro-linux/html/

@dlwillson:

- https://en.wikibooks.org/wiki/Bash_Shell_Scripting